## [1.2.3](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.2.2...v1.2.3) (2021-03-28)


### Bug Fixes

* force version tag ([af6e226](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/af6e22657d3dc30c367d4b1845857abae810bcb7))

## [1.2.2](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.2.1...v1.2.2) (2021-03-28)


### Bug Fixes

* dns handling ([cc80cc2](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/cc80cc292467d5171182043bdbc8af5830cf0a7f))
* update cluster config ([a18be27](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/a18be2785280b3f26c0349a6cc3d976c51285db5))

## [1.2.1](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.2.0...v1.2.1) (2021-03-25)


### Bug Fixes

* clustering startup script ([b3c49b3](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/b3c49b37c64e6ecc501b67c4cc75d813f5c0ba2d))

# [1.2.0](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.1.3...v1.2.0) (2021-03-25)


### Bug Fixes

* add health check ([3b7fd32](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/3b7fd32e9e5f384f398320a416226652eae9d66f))
* add target group arn output ([cfec709](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/cfec709ba0f88846e77a508cb55dce91afc01622))
* allow custom user data ([409ebf8](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/409ebf810ba9268305aafaf9719407b6ffa16d83))
* bash script ([248af98](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/248af9855370ceb5eecf308e75409c5133d8bfbe))
* create zip files for lambda and layers ([b195577](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/b195577cbf2cf6b5985b6d5f233166e44351f69b))
* dns handling for instance names ([a672f1b](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/a672f1b9fb3ef3ab012faf7a5840ad3eda2279c8))
* escape vars ([b580420](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/b5804206d7c50fc0d575d8f5672cdbb8670fb219))
* evenstore conf ([5223dec](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/5223dec9dca6c6ea4106769db768b03d7e39438e))
* file ownership ([c3db007](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/c3db007ad180e2edc0b8f7453c2eebf82748215b))
* path to zip files ([4f1a2ba](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/4f1a2bafee5895630ae2dd0ad30c32d56c6da0ca))
* protocol enum ([a8f1474](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/a8f14742290f0aada3494e4b87a7856b123eef5d))
* protocol HTTP ([88bf991](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/88bf99177c78a6cf2005d7a7fa9b3b7e028270fd))
* remove wait_for_elb_capacity ([66a77e3](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/66a77e3f0ae9351d015136e7b7126ed11b64f2f4))
* revery target group port ([55740eb](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/55740eb2d13a3d6ffdae26ad8845c81812e0108b))
* startup script ([41efd7f](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/41efd7f79e44d54aa027023ada2d975d13f3d45b))
* update eventstore conf ([b51efe8](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/b51efe835fb07d745917ca9d21d66bcbad589c65))
* update user_data ([4ea9dc3](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/4ea9dc3b310ed48824a1a031d267aab352b6689c))
* update user_data ([7100343](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/7100343099a58c946b0b9fad2c25fb5cfa0bd7ae))
* user data script ([1f443f1](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/1f443f16638410ab15a779eb7070057b28c0eeb9))
* wait_for_elb_capacity ([6f6e806](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/6f6e8061b4b04b0661627512b25b62bd78fe7bf9))


### Features

* handle dns updating for cluster asg ([47c04ee](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/47c04eece0de30f153bbca9bfe68546f892c0126))

## [1.1.3](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.1.2...v1.1.3) (2020-10-26)


### Bug Fixes

* dns handling for instance names ([432ee3a](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/432ee3ac46981ce5a013d0d6139e99231b91d18d))

## [1.1.2](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.1.1...v1.1.2) (2020-10-16)


### Bug Fixes

* path to zip files ([438b5fe](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/438b5fedbedf14d7a8f58ae30203f6d65fc07239))

## [1.1.1](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.1.0...v1.1.1) (2020-10-16)


### Bug Fixes

* bash script ([66af875](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/66af875f07d953e5b900a927031dc54caf43a42e))
* create zip files for lambda and layers ([67e598f](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/67e598f9dc09dc89699eb0db86e7a8bcda0ebde3))

# [1.1.0](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.16...v1.1.0) (2020-10-16)


### Features

* handle dns updating for cluster asg ([57d1a4b](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/57d1a4b58cfe422f45a4491276bd2382607cad1c))

## [1.0.16](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.15...v1.0.16) (2020-10-15)


### Bug Fixes

* escape vars ([c322c70](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/c322c70db4017d11229d566377852f5c7d84a9e8))

## [1.0.15](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.14...v1.0.15) (2020-10-15)


### Bug Fixes

* update user_data ([55741f8](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/55741f8b0b407e5c485679f433bc5850993cc4cf))

## [1.0.14](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.13...v1.0.14) (2020-10-15)


### Bug Fixes

* file ownership ([312bf11](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/312bf112b2d2d406a14b419fcbda27b974dfdc4b))

## [1.0.13](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.12...v1.0.13) (2020-10-15)


### Bug Fixes

* startup script ([77eb4e4](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/77eb4e410dbda2d4b2141d921ff4d61712336302))

## [1.0.12](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.11...v1.0.12) (2020-10-15)


### Bug Fixes

* revery target group port ([9c46c81](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/9c46c8139b2bf356306c5667e03d2b3bd6fa1b3a))

## [1.0.11](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.10...v1.0.11) (2020-10-15)


### Bug Fixes

* add health check ([fb7b003](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/fb7b00326c56d9299f778b9a666ab58e816faa25))

## [1.0.10](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.9...v1.0.10) (2020-10-15)


### Bug Fixes

* evenstore conf ([599174d](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/599174dea92c376f2a303812c1ed437db8ca7885))

## [1.0.9](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.8...v1.0.9) (2020-10-15)


### Bug Fixes

* allow custom user data ([aa524f9](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/aa524f9d00f889a1cbd7e83e9ff0c7910f3ee972))
* update eventstore conf ([67dadc4](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/67dadc411c0b132d589b7a3161f8ee2d3102c6e2))

## [1.0.8](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.7...v1.0.8) (2020-10-15)


### Bug Fixes

* update user_data ([c01413f](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/c01413fa705b052470dfcdfff206ca8048d355e2))

## [1.0.7](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.6...v1.0.7) (2020-10-15)


### Bug Fixes

* user data script ([9c09029](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/9c09029ca9f2040ba96ca86e1300d94081869caf))

## [1.0.6](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.5...v1.0.6) (2020-10-15)


### Bug Fixes

* protocol HTTP ([aa8902b](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/aa8902b5baef657a501b404a5de6813f117996df))

## [1.0.5](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.4...v1.0.5) (2020-10-15)


### Bug Fixes

* protocol enum ([2ea08d2](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/2ea08d260b3122191493f7739a50cb2cd2af6b3c))

## [1.0.4](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.3...v1.0.4) (2020-10-15)


### Bug Fixes

* add target group arn output ([34d67f2](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/34d67f221ae5081b4809d9f51f68a256653fdde0))

## [1.0.3](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.2...v1.0.3) (2020-10-15)


### Bug Fixes

* remove wait_for_elb_capacity ([bf6fe6a](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/bf6fe6a3ce7be7ac6f7ea644cf4c473a7c5dbb3c))

## [1.0.2](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.1...v1.0.2) (2020-10-15)


### Bug Fixes

* wait_for_elb_capacity ([9d766a4](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/9d766a46bed2e408d179a34aff614bda47d164f7))

## [1.0.1](https://bitbucket.org/plsos2/terraform-aws-eventstore/compare/v1.0.0...v1.0.1) (2020-10-15)


### Bug Fixes

* wait_for_elb_capacity ([854ba38](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/854ba38df67d35ba6c96a6486ccc8178230637d3))

# 1.0.0 (2020-10-15)


### Bug Fixes

* initial commit ([961c8ac](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/961c8aca99decb87e7af902aa2bd1743a42831c5))
* remove provider ([ef8a03e](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/ef8a03e35936eac14fea9087f3eaef62cb33999e))
* version 12 requirement ([18fa23b](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/18fa23b242f23a10226661169021f8219f2077fe))


### Features

* add target groups ([d011870](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/d011870f9c0320975a620986371dd0b6be6fbd68))
* aws provider 3.x ([604290b](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/604290b285b9fc463c9fc384c54471bf318be4a3))
* upgrade to 0.13 ([6ef89df](https://bitbucket.org/plsos2/terraform-aws-eventstore/commits/6ef89df021ec3b19643d5fb36913e17e0b45bf22))

terraform {
  backend "s3" {
    bucket  = "proplogix-terraform"
    encrypt = true
    key     = "terraform-aws-eventstore-aws.tfstate"
    profile = "proplogix"
    region  = "us-east-1"
  }

  required_providers {
    archive  = "~> 2.0"
    aws      = "~> 2.70.0"
    external = "~> 2.0"
    local    = "~> 1.0"
    random   = "~> 3.0"
    template = "~> 2.0"
    tls      = "~> 2.0"
  }
}

locals {
  availability_zones = ["us-east-2a", "us-east-2b"]
  domain             = "plx-dev.net"
  environment        = "test"
  project_name       = "eventstore-test"
  region             = "us-east-2"
}

provider "aws" {
  region  = local.region
  profile = "proplogix"
}

module "domain" {
  source = "git::https://bitbucket.org/plsos2/terraform-modules-aws//aws/domain?ref=v6.3.5"
  domain = local.domain
}

module "env" {
  source = "git::https://bitbucket.org/plsos2/terraform-modules-aws//common/env?ref=v6.3.5"
  domain = local.domain
}

module "bucket" {
  source         = "git::https://bitbucket.org/plsos2/terraform-modules-aws//aws/bucket?ref=v6.3.5"
  domain         = module.env.domain_name
  bucket_name    = "${local.project_name}.${module.env.domain_name}"
  principal_arns = [module.user.arn]
  project_name   = local.project_name
}

module "keypair" {
  source       = "git::https://bitbucket.org/plsos2/terraform-modules-aws//aws/keypair?ref=v6.3.5"
  bucket_name  = module.bucket.bucket_name
  domain       = module.env.domain_name
  project_name = local.project_name
}

module "user" {
  source       = "git::https://bitbucket.org/plsos2/terraform-modules-aws//aws/user?ref=v6.3.5"
  domain       = module.env.domain_name
  group_name   = local.project_name
  project_name = local.project_name
  user_name    = local.environment
}

module "vpc" {
  source                         = "git::https://bitbucket.org/plsos2/terraform-modules-aws//aws/vpc?ref=v6.3.5"
  availability_zones             = local.availability_zones
  environment                    = local.environment
  project_name                   = local.project_name
  vpc_cidr_block                 = "172.100.0.0/16"
  vpc_cidr_private_block_subnets = ["172.100.16.0/20", "172.100.32.0/20"]
  vpc_cidr_public_block_subnets  = ["172.100.128.0/20", "172.100.144.0/20"]
}

module "eventstore" {
  source               = "../"
  cluster_azs          = local.availability_zones
  cluster_dns          = "${local.project_name}.${local.domain}"
  cluster_name         = local.project_name
  cluster_subnets      = module.vpc.public.*.id
  cluster_version      = "20.10.0"
  cluster_vpc_id       = module.vpc.vpc_id
  environment          = local.environment
  key_pair_name        = local.project_name
  key_pair_publickey   = module.keypair.public_key_openssh
  instance_type        = module.env.is_production ? "t2.medium" : "t2.micro"
  instance_volume_size = module.env.is_production ? 8 : 16
  region               = local.region
  user_arn             = module.user.arn

  vpc_name = module.vpc.vpc_id
  zone_id  = module.domain.zone_id
}
